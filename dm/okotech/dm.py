"""

Deformable mirror class that implements the same API as dm/thorlabs/dm.ThorlabsDM

"""

import dm.okotech.okodm_sdk.python.okodm as okodm

class OkoDM:
    def __init__(self, dmtype):
        
        types=[
            ("MMDM 37ch,15mm","USB DAC 40ch, 12bit",["D40V2e15"]),
            ("MMDM 96ch, embedded control","Embedded HV DAC"),
            ("MMDM 79ch,30mm","USB DAC 40ch, 12bit")
        ]
        
        self.h=okodm.open(*types[dmtype])
        
        if self.h==0:
            raise ConnectionError(f"Error opening OKODM device: {okodm.lasterror()}")
            
        self.n=okodm.chan_n(self.h)
        
    def __len__(self):
        return self.n
    
    def setActuators(self, act):
        
        assert len(act) == len(self)
        
    def getActuators(self):
        return
    
    def close(self):
        if self.h != 0:
            okodm.close(self.h)
            self.h = None
    
    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()
    
if __name__ == "__main__":
    with OkoDM(dmtype=1) as dm:
        print(f"Oko DM has {len(dm)} actuators")