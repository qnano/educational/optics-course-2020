import okodm
import time
import sys

#h=okodm.open("MMDM 37ch,15mm","USB DAC 40ch, 12bit",["D40V2e15"])
h=okodm.open("MMDM 96ch, embedded control","Embedded HV DAC")
#h=okodm.open("MMDM 79ch,30mm","USB DAC 40ch, 12bit")

if h==0:
    print(f"Error opening OKODM device: {okodm.lasterror()}")
else:
    n=okodm.chan_n(h)
    stage=1   
       
    try:
        while True:
            if not okodm.set(h, ([1] if stage else [-1])*n):
                print(f"Error writing to OKODM device: {okodm.lasterror()}")
                break
            time.sleep(1)
            stage^=1
            
    except KeyboardInterrupt:
        pass        
      
    okodm.close(h)  
    
