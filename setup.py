import setuptools
import os,shutil


long_description = """
DCSC Optics course 2020 package
"""

pk = setuptools.find_packages()

setuptools.setup(
    name="ao2020",
    version="0.1",
    author="Jelmer Cnossen",
    author_email="j.p.cnossen@tudelft.nl",
    description="DCSC Optics course 2020 package",
    long_description=long_description,
 #   long_description_content_type="text/markdown",
    url="",
    packages=pk,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Microsoft :: Windows"
    ],
	install_requires=[
		'pyueye',
		'scipy', 
		'numpy',
		'matplotlib',
		'tqdm',
		'tifffile',
        'h5py',
		'pyyaml',
        'napari',
        'scikit-image',
        'pythonnet'
	]
)
